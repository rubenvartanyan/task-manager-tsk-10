package ru.vartanyan.tm.constant;

public interface TerminalConstant {

    String CMD_HELP = "help";
    String CMD_VERSION = "version";
    String CMD_ABOUT = "about";
    String CMD_EXIT = "exit";
    String CMD_INFO = "info";
    String CMD_ARGUMENTS = "arguments";
    String CMD_COMMANDS = "commands";
    String CMD_TASK_CREATE = "task-create";
    String CMD_TASK_CLEAR = "task-clear";
    String CMD_TASK_LIST = "task-list";
    String CMD_TASK_REMOVE = "task-remove";
    String CMD_PROJECT_CREATE = "project-create";
    String CMD_PROJECT_CLEAR = "project-clear";
    String CMD_PROJECT_LIST = "project-list";
    String CMD_PROJECT_REMOVE = "project-remove";

}
