package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.IProjectRepository;
import ru.vartanyan.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public void add(final Project project) {
        list.add(project);
    }

    @Override
    public void remove(final int index) {
        list.remove(index);
    }

    @Override
    public void clear() {
        list.clear();
    }

}
