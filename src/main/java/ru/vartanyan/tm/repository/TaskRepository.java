package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.ITaskRepository;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public void add(final Task task){
        list.add(task);
    }

    @Override
    public void remove(final int index) {
        list.remove(index);
    }

    @Override
    public void clear() {
        list.clear();
    }

}
