package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.ICommandRepository;
import ru.vartanyan.tm.constant.ArgumentConstant;
import ru.vartanyan.tm.constant.TerminalConstant;
import ru.vartanyan.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConstant.CMD_ABOUT, ArgumentConstant.ARG_ABOUT, "Show developer info."
    );

    private static final Command HELP = new Command(
            TerminalConstant.CMD_HELP, ArgumentConstant.ARG_HELP, "Show terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.CMD_VERSION, ArgumentConstant.ARG_VERSION, "Show application version."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.CMD_EXIT, null, "Close application."
    );

    private static final Command INFO = new Command(
            TerminalConstant.CMD_INFO, ArgumentConstant.ARG_INFO, "Show system info."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.CMD_ARGUMENTS, null, "Show program arguments."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.CMD_COMMANDS, null, "Show program command"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConstant.CMD_TASK_CREATE, null, "create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConstant.CMD_TASK_CLEAR, null, "clear all tasks"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConstant.CMD_TASK_LIST, null, "show task list"
    );

    private static final Command TASK_REMOVE = new Command(
            TerminalConstant.CMD_TASK_REMOVE, null, "remove task by index"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConstant.CMD_PROJECT_CREATE, null, "create new project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConstant.CMD_TASK_CLEAR, null, "clear all projects"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConstant.CMD_PROJECT_LIST, null, "show project list"
    );

    private static final Command PROJECT_REMOVE = new Command(
            TerminalConstant.CMD_PROJECT_REMOVE, null, "remove project by index"
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, EXIT, INFO, ARGUMENTS, COMMANDS,
            TASK_CREATE, TASK_CLEAR, TASK_LIST, TASK_REMOVE,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, PROJECT_REMOVE
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    };

}
