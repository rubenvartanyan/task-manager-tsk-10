package ru.vartanyan.tm.controller;

import ru.vartanyan.tm.api.IProjectController;
import ru.vartanyan.tm.api.IProjectService;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {

        System.out.println("[PROJECT LIST]");
        final List<Project> list = projectService.findAll();
        int index = 0;
        for (final Project project: list) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");

    }

    @Override
    public void create() {

        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER MAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[PROJECT CREATED]");

    }

    @Override
    public void clear() {

        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");

    }

    @Override
    public void remove() {

        System.out.println("[PROJECT REMOVE]");
        System.out.println("[ENTER INDEX]");
        String indexString = TerminalUtil.nextLine();
        int index = Integer.parseInt(indexString);
        try {
            projectService.remove(index);
            System.out.println("[PROJECT REMOVED]");
        } catch (Exception e) {
            System.out.println("Error! No project with such index...");
        }
    }

}
