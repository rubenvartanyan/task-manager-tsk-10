package ru.vartanyan.tm.controller;

import ru.vartanyan.tm.api.ITaskController;
import ru.vartanyan.tm.api.ITaskService;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {

        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 0;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");

    }

    @Override
    public void create() {

        System.out.println("[TASK CREATE]");
        System.out.println("ENTER MAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[TASK CREATED]");

    }

    @Override
    public void clear() {

        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");

    }

    @Override
    public void remove() {

        System.out.println("[TASK REMOVE]");
        System.out.println("[ENTER INDEX]");
        String indexString = TerminalUtil.nextLine();
        int index = Integer.parseInt(indexString);
        try {
            taskService.remove(index);
            System.out.println("[TASK REMOVED]");
        } catch (Exception e) {
            System.out.println("Error! No task with such index...");
        }
    }

}
