package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface ITaskRepository {
    List<Task> findAll();

    void add(Task task);

    void remove(int index);

    void clear();
}
