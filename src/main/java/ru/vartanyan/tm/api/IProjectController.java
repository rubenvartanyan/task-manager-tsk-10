package ru.vartanyan.tm.api;

public interface IProjectController {
    void showList();

    void create();

    void clear();

    void remove();
}
