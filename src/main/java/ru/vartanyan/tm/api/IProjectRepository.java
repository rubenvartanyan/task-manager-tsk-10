package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.Project;

import java.util.List;

public interface IProjectRepository {
    List<Project> findAll();

    void add(Project project);

    void remove(int index);

    void clear();
}
