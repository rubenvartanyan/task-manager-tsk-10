package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.ITaskRepository;
import ru.vartanyan.tm.api.ITaskService;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final int index) {
        taskRepository.remove(index);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task add(String name, String description) {

        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;

    }

}
