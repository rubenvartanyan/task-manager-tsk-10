package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.IProjectRepository;
import ru.vartanyan.tm.api.IProjectService;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(Project project) {

    }

    @Override
    public void remove(final int index) {
        projectRepository.remove(index);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project add(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;

    }

}
