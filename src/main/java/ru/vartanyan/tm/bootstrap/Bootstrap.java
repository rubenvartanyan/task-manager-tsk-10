package ru.vartanyan.tm.bootstrap;

import ru.vartanyan.tm.api.*;
import ru.vartanyan.tm.constant.ArgumentConstant;
import ru.vartanyan.tm.constant.TerminalConstant;
import ru.vartanyan.tm.controller.CommandController;
import ru.vartanyan.tm.controller.ProjectController;
import ru.vartanyan.tm.controller.TaskController;
import ru.vartanyan.tm.repository.CommandRepository;
import ru.vartanyan.tm.repository.ProjectRepository;
import ru.vartanyan.tm.repository.TaskRepository;
import ru.vartanyan.tm.service.CommandService;
import ru.vartanyan.tm.service.ProjectService;
import ru.vartanyan.tm.service.TaskService;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);


    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);


    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService);


    public void run(final String... args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true){
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConstant.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConstant.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConstant.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConstant.ARG_INFO: commandController.showInfo(); break;
            default: showIncorrectArg();
        }
    }


    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    public void parseCommand(final String arg) {
        if (arg == null) return;
        switch (arg){
            case TerminalConstant.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConstant.CMD_HELP: commandController.showHelp(); break;
            case TerminalConstant.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConstant.CMD_INFO: commandController.showInfo(); break;
            case TerminalConstant.CMD_EXIT: commandController.exit(); break;
            case TerminalConstant.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConstant.CMD_ARGUMENTS: commandController.showArguments(); break;
            case TerminalConstant.CMD_TASK_CREATE: taskController.create(); break;
            case TerminalConstant.CMD_TASK_LIST: taskController.showList(); break;
            case TerminalConstant.CMD_TASK_CLEAR: taskController.clear(); break;
            case TerminalConstant.CMD_TASK_REMOVE: taskController.remove(); break;
            case TerminalConstant.CMD_PROJECT_CREATE: projectController.create(); break;
            case TerminalConstant.CMD_PROJECT_LIST: projectController.showList(); break;
            case TerminalConstant.CMD_PROJECT_CLEAR: projectController.clear(); break;
            case TerminalConstant.CMD_PROJECT_REMOVE: projectController.remove(); break;
            default: showIncorrectCommand();
        }
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }


}
